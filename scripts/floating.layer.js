/*
 * �������
 * ��;�� 1�����㸡�����λ�ã�2������������������ظ���
 */

var FLOATING_LAYER = {
    key: "FLOATING_LAYER_KEY",
    list: [], // { id: this.aid, show: false }
    init: function () {
        this.addEvent(); // ע�� document �¼�
    },
    addEvent: function () {
        document.addEventListener("mousedown", this.click);
        document.addEventListener("touchstart", this.click);
    },
    click: function ($event) {
        var self = FLOATING_LAYER;
        if ($event !== undefined) {
            $event.stopPropagation();
        }
        self.hide(getElement($event.target));
        function getElement(element) {
            if (!element) {
                return "";
            }
            var eid = element.id;
            if (eid && eid.length > 0) {
                var id = eid.replace(self.key, '');
                if (self.list.indexOf(id) >= 0)
                    return id;
            }
            return getElement(element.offsetParent);
        }
    },
    hide: function (id) {
        var arr = this.list;
        for (var i = 0; i < arr.length; i++) {
            if (id.length > 0 && arr[i] === id) { continue; }
            var elem = document.getElementById(this.key + arr[i]);
            if (elem) { elem.style.display = "none"; }
        }
    },
    getElementOffSet: function (element) {
        return { top: getElementTop(element), left: getElementLeft(element) };

        function getElementTop(element) {
            var actualTop = element.offsetTop;
            var current = element.offsetParent;
            while (current !== null) {
                actualTop += current.offsetTop;
                current = current.offsetParent;
            }
            return actualTop;
        }
        function getElementLeft(element) {
            var actualLeft = element.offsetLeft;
            var current = element.offsetParent;
            while (current !== null) {
                actualLeft += current.offsetLeft;
                current = current.offsetParent;
            }
            return actualLeft;
        }
    },
    guid: function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
};

// ��ʼ������
FLOATING_LAYER.init();